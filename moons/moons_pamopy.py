#coding utf-8

import sys
import torch
from torch.utils.data import Dataset, DataLoader
from pamopy.loaders import load_moons
from pamopy.classicalmethods import *
from pathlib import Path

rep =  Path(sys.argv[-1]) #Path("/Users/lusenn/Desktop/donnees-ml/")
train_set, test_set = load_moons(rep,create=False)

numbers_of_neighbors = range(1,101)
for nb_neighbor in numbers_of_neighbors:
    guessed_counter = 0
    for X_to_locate, true_target in test_set:
        dist_func = torch.linalg.norm
        guessed_target = label_by_knn( X_to_locate, train_set.data, train_set.targets, nb_neighbor, lambda x: torch.linalg.norm(x, dim=(1,)) )
        if guessed_target == true_target:
            guessed_counter += 1
    percentage_of_success = (guessed_counter / len(test_set)) * 100
    print(" percentage of success for {} nearest neighbors : {:.2f} % ".format(nb_neighbor, percentage_of_success ) )
