#coding: utf-8

import numpy as npy
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons
import pickle
import time

from torch.utils.data import Dataset, DataLoader
import torch.nn as nn
import torch.nn.functional as F
import torch

#######################################################
# Code for the half-moon test of parrhesie.fr :
# https://www.parrhesie.fr/2021/02/21/premier-reseau-neuronal/
#
# Classicaly, first part of the code are the definition of the function
# last part is the script
#######################################################



###############
# graphical
###############
def plot_data(ax, X, colors):
    """
    utility function to do scattering plot of data belonging to different set
    Input :
    -----
    X : (N,2) numpy array of points to scatter
    colors : N length iterable of the color to use for each point
    """
    plt.axis('off')
    ax.scatter(X[:, 0], X[:, 1], s=1, c=colors, cmap='bone')
###############
# end graphical
###############


####################
# Classical methods
####################
def which_moons_mean(X_to_locate,X_train,label):
    """
    determine to which moons belongs the given point X_to_locate,
    given samples X_train and the corresponding label, by determining
    the distance at "mean moon" of X_to_locate
    ----
    Inputs :
    X_to_locate : a (2,) numpy array of floats
    X_train :  (N,2) numpy array of floats
    label : (N,2) numpy array of 0 and 1
    ------
    Output : 0 (X_to_locate belongs to 0-labelled moon) or 1  0 (X_to_locate belongs to 1-labelled moon)
    """
    # 0-labelled moon will be referred as M0
    # 1-labelled moon will be referred as M1    
    X0 = X_train[ label==0, : ] # samples points belonging to M0
    X1 = X_train[ label==1, : ] # samples points belonging to M1
    # computation of d0, the distance of X_to_locate to the mean of M0
    d0 = npy.mean(npy.sqrt(npy.sum(npy.abs(X0 - X_to_locate)**2 , axis=1 )))
    # computation of d1, the distance of X_to_locate to the mean of M1
    d1 = npy.mean(npy.sqrt(npy.sum(npy.abs(X1 - X_to_locate)**2 , axis=1 )))
    if d0<d1:
        return 0 # d0 is smaller than d1, X_to_locate belongs to M0
    else:
        return 1 # d1 is smaller than d0, X_to_locate belongs to M1

    
def which_moons_closest(X_to_locate,X_train,label):
    """
    determine to which moons belongs the given point X_to_locate,
    given samples X_train and the corresponding label, by determining
    the closest element to X_to_locate
    ----
    Inputs :
    X_to_locate : a (2,) numpy array of floats
    X_train :  (N,2) numpy array of floats
    label : (N,2) numpy array of 0 and 1
    ------
    Output : 0 (X_to_locate belongs to 0-labelled moon) or 1  0 (X_to_locate belongs to 1-labelled moon)

    """
    # 0-labelled moon will be referred as M0
    # 1-labelled moon will be referred as M1    
    X0 = X_train[ label==0, : ] # samples points belonging to M0
    X1 = X_train[ label==1, : ] # samples points belonging to M1
    # computation of d0, the distance of X_to_locate to the closest point in M0
    d0 = npy.min( npy.sqrt(npy.sum(npy.abs(X0 - X_to_locate)**2 , axis=1 ) ))
    # computation of d1, the distance of X_to_locate to the closest point in M1
    d1 = npy.min(npy.sqrt(npy.sum(npy.abs(X1 - X_to_locate)**2 , axis=1 )))
    if d0<=d1:
        return 0 # d0 is smaller than d1, X_to_locate belongs to M0
    else:
        return 1 # d1 is smaller than d0, X_to_locate belongs to M1
###########################
# end of classical methods
##########################

###################
# ml method
###################
class MoonsDataset(Dataset):
    """half-moons points dataset"""
    def __init__(self, points, labels ):
        self.points = torch.from_numpy(points)
        self.labels = torch.from_numpy(labels)

    def __len__(self):
        return len(self.points)

    def __getitem__(self,idx):
        return self.points[idx], self.labels[idx]

    
class ClassifierSoftmax(nn.Module):
    """
    A Classifier for the half-moons classification problem.
    Input parameter is of size 2, as it's a point of the plan.
    This input parameter goes trough a fully connected linear layer, of output dimension N
    Then we go trough a ReLU activation function.
    Then, we go trough a fully connected linear of output dimension 2, as we want to separate between classes.
    We use a log_softmax function as we're goint to use a NLLLoss.
    """
    def __init__(self, layer_size ):
        super(ClassifierSoftmax, self).__init__()
        self.block1 = nn.Sequential( nn.Linear( 2, layer_size ), 
                                     nn.ReLU(),
                                     nn.Linear( layer_size, 2) )

    def initialize(self):
        nn.init.xavier_uniform(self.linear.weight.data)
        self.linear.bias.data.zero_()

    def forward(self,x):
        x1 = self.block1(x)
        x1 = x1.view( x1.size(0), -1 )
        return F.log_softmax( x1 ,dim=0)

    def output_to_label(self,output):
        """ Convert an outptut to a label of the classification problem """
        for out in output:
            out = torch.exp(out)
        _,label = torch.max(output,1)
        return label

        
class ClassifierSigmoid(nn.Module):
    """
    A Classifier for the half-moons classification problem.
    Input parameter is of size 2, as it's a point of the plan.
    This input parameter goes trough a fully connected linear layer, of output dimension N
    Then we go trough a ReLU activation function.
    Then, we go trough a fully connected linear of output dimension 2, as we want to separate between classes.
    We use a log_softmax function as we're goint to use a NLLLoss.
    """
    def __init__(self, layer_size ):
        super(ClassifierSigmoid, self).__init__()
        self.block1 = nn.Sequential( nn.Linear( 2, layer_size ), 
                                     nn.ReLU(),
                                     nn.Linear( layer_size, 1) )

    def initialize(self):
        nn.init.xavier_uniform(self.linear.weight.data)
        self.linear.bias.data.zero_()

    def forward(self,x):
        x1 = self.block1(x)
        x1 = x1.view( x1.size(0), -1 )
        return torch.sigmoid(x1)

    def output_to_label(self,output):
        """ Convert an outptut to a label of the classification problem """
        return torch.round(output).flatten()



        
    
def train(*, model, dataloader, loss_fn, optimizer, n_epochs=1):
    """ 
    train a model with data given by a dataloader, according to a loss function and an optimizer
    Inputs :
    - model : a pytorch model
    - dataloader : the dataloader to load training data set (data + label)
    - loss_fn : the loss function to use
    - optimizer :the optimizer to use
    - n_epochs : the number of loop to do in the optimization process
    """
    model.train(True)
    loss_by_epoch = npy.zeros(n_epochs) # to stock the mean loss for each step of the loop
    correct_pred_by_epoch = npy.zeros(n_epochs) # to stock the mean accuracy for each step of the loop
    for epoch_num in range(n_epochs):
        percentage_correct_prediction_by_batch = 0.0 
        mean_loss_by_batch = 0.0
        size = 0
        for inputt,label in dataloader: # we loop on the batches
            output = model(inputt.float()) # we compute the output of the model
            loss = loss_fn( output, label)  # we compute the loss
            optimizer.zero_grad() # gradient set to zero
            loss.backward() # backpropagation of the gradient
            optimizer.step()
            mean_loss_by_batch += loss.item()
            preds = model.output_to_label( output )
            percentage_correct_prediction_by_batch += torch.sum( preds == label ).item() # we add the number of correct prediction in the batch
            size += 1
        mean_loss_by_batch /= size
        percentage_correct_prediction_by_batch /=  (size * dataloader.batch_size ) / 100 
        loss_by_epoch[epoch_num] = mean_loss_by_batch
        correct_pred_by_epoch[epoch_num] = percentage_correct_prediction_by_batch
        print(' mean loss | mean correction prediction :  {:.4f} | {:.4f} '.format(mean_loss_by_batch, percentage_correct_prediction_by_batch))
    return loss_by_epoch, correct_pred_by_epoch



def test_model(model, dataloader, loss_fn):
    model.eval()
    mean_loss_by_batch = 0.0
    percentage_correct_prediction_by_batch = 0.0 
    size = 0
    for inputt, label in dataloader:
        output = model(inputt.float())
        loss = loss_fn( output,label )
        preds = model.output_to_label( output )
        # statistics
        mean_loss_by_batch += loss.data.item()
        percentage_correct_prediction_by_batch += torch.sum(preds == label).item()
        size += 1
    mean_loss_by_batch /=  size 
    percentage_correct_prediction_by_batch /= (size * dataloader.batch_size )/100
    print('Loss: {:.4f} % of correct: {:.4f}'.format( mean_loss_by_batch, percentage_correct_prediction_by_batch ))
    #return predictions, all_proba, all_classes
###################
# ml method
###################

        
        
if __name__ == "__main__":
    save_bool = False # to save or not figures and model weights after training
    ##################################
    # loading/generating data
    ##################################
    print(" loading or generating data ")
    # the training set
    try:
        with open("train.pck","rb") as f:            
            X_train, label_train = pickle.load(f)
    except FileNotFoundError:
        print(" training set doesn't exist. We generate and save it " )
        X_train, label_train = make_moons(n_samples=2000, noise=0.1)
        with open("train.pck","wb") as f:
            pickle.dump([X_train, label_train], f)
    # end of training set
    # the test set
    try:
        with open("test.pck","rb") as f:            
            X_test, label_test = pickle.load(f)
    except FileNotFoundError:
        print( " test set doesn't exit. We generate and save it ")
        X_test, label_test = make_moons(n_samples=10000, noise=0.1)
        with open("test.pck","wb") as f:
            pickle.dump([X_test, label_test], f )
    # end of the test set
    # now we convert the numpy data to pytorch Dataset and Dataloader
    points_dataset = {"train":X_train,"test":X_test}
    labels_dataset = {"train":label_train,"test":label_test}
    dsets = { x:MoonsDataset( points_dataset[x], labels_dataset[x] )  for x in ["train","test"]}
    train_loader = torch.utils.data.DataLoader(dsets['train'], batch_size=100, shuffle=True, num_workers=4)
    test_loader = torch.utils.data.DataLoader(dsets['test'], batch_size=100, shuffle=True, num_workers=4)
    print(" data generated or loaded, dataset and dataloader constructed " )
    ##################################
    # loading/generating data
    ##################################


        
    #########################################
    # Classification using classical methods
    #########################################
    mean_counter = 0 # counting the number of success with the mean method
    closest_counter = 0 # counting the number of success with the closest method
    for X_to_locate, true_label in zip(X_test,label_test): # loop on the testing set
        label_mean = which_moons_mean( X_to_locate, X_train, label_train ) # label found by the mean method
        label_closest = which_moons_closest( X_to_locate, X_train, label_train ) # label found by the closest method
        if label_mean == true_label: 
            mean_counter += 1 # if the label found by "mean" equal the true_label, we increase the number of success
        if label_closest == true_label:
            closest_counter += 1 # if the label found by "closest" equal the true_label, we increase the number of success            
    print(" percentage of success for mean-method = ",mean_counter/len(label_test)*100) 
    print(" percentage of success for closest-method = ",closest_counter/len(label_test)*100)
    fig = plt.figure()
    ax = fig.add_subplot(111, facecolor='#000000')
    plot_data(ax,X_test, [ "k" if y == 1 else "y" for y in label_test ] )
    plot_data(ax,X_train, [ "r" if y == 1 else "b" for y in label_train ] )
    if save_bool:
        fig.savefig("classification_classique.png",transparent=True)
        fig.clf()        
    ###############################################
    # End of classification using classical methods
    ################################################

    
    n_epochs=100
    #########################################################
    # Classification using machine learning : NLLLoss / Adam
    #########################################################
    print("----------------------")
    print(" We begin classification using NLLLoss and Adam optimizer")
    N = 5
    classifier_NLL = ClassifierSoftmax(N)
    optim = torch.optim.Adam(classifier_NLL.parameters(), 0.1 )
    loss_fn = nn.NLLLoss()
    try:
        classifier_NLL.load_state_dict(torch.load("classifier_NLL_Adam_"+str(N)+".pthh")) # first we try to see if we already train the model   
    except FileNotFoundError: # if we didn't find saved weights, we train the model
        print(" we train ")
        initial_time = time.time()
        train( model=classifier_NLL,
               dataloader=train_loader,
               loss_fn=loss_fn,
               optimizer=optim,
               n_epochs=n_epochs) # number of epochs choosen arbitrarily
        print(" training finished ")
        computation_time_NLL = time.time() - initial_time
        if save_bool:
            torch.save(classifier_NLL.state_dict(),"classifier_NLL_Adam_" +str(N) + ".pth")

    # now we validate the model
    test_model( classifier_NLL, test_loader, loss_fn )
    ################################################################
    # End of classification using machine learning : NLLLoss / Adam
    ################################################################


    #########################################################
    # Classification using machine learning : BCELoss / Adam
    #########################################################
    bce_loss = nn.BCELoss()
    def my_bce_loss( output, label ):
        return bce_loss( output, label.float().unsqueeze(1) )
    
    print("----------------------")
    print(" We begin classification using BCELoss and Adam optimizer")
    N = 5
    classifier_BCE = ClassifierSigmoid(N)
    optim = torch.optim.Adam(classifier_BCE.parameters(), 0.1 )
    loss_fn = my_bce_loss
    try:
        classifier_BCE.load_state_dict(torch.load("classifier_BCE_Adam_"+str(N)+".pthh")) # first we try to see if we already train the model   
    except FileNotFoundError: # if we didn't find saved weights, we train the model
        print(" we train ")
        initial_time = time.time()
        train( model=classifier_BCE,
               dataloader=train_loader,
               loss_fn=loss_fn,
               optimizer=optim,
               n_epochs=n_epochs) # number of epochs choosen arbitrarily
        print(" training finished ")
        computation_time_BCE = time.time() - initial_time
        if save_bool:
            torch.save(classifier_BCE.state_dict(),"classifier_BCE_Adam_"  +str(N) + ".pth")

    # now we validate the model
    test_model( classifier_BCE, test_loader, loss_fn )
    ################################################################
    # End of classification using machine learning : BCELoss / Adam
    ################################################################

    
    ################################################################
    # Graphics to perceive the delimiters
    ################################################################    
    # we use, probably not in the more elegant way, a graphical plot to see the area delimitation
    # first we build in a numpy way
    x_min, x_max = -1.5, 2.5
    y_min, y_max = -1, 1.5
    X = npy.linspace(x_min,x_max,101)
    Y = npy.linspace(y_min,y_max,103)
    XX,YY = npy.meshgrid(X,Y)    
    ZZ_NLL = npy.zeros(XX.shape) # if (x,y) is in 0 region 0, will be set to 0. If (x,y) in 1, set to 1
    ZZ_BCE= npy.zeros(XX.shape) # if (x,y) is in 0 region 0, will be set to 0. If (x,y) in 1, set to 1
    # now that we build the numpy structure, we build the pytorch
    XXYY =  npy.ascontiguousarray( npy.array( [ XX.flatten(), YY.flatten() ] ).T ) # we flatten to iterate; and set as contiguous
    graph_dset = MoonsDataset( XXYY, npy.nan*npy.zeros(len(XXYY)) ) # we build the dataset, with no labels
    graph_loader = torch.utils.data.DataLoader(graph_dset, batch_size=len(XXYY), shuffle=False, num_workers=4) # we build the dataloader, with a batch-size of all points
    for inputt,_ in graph_loader: # actually just one step in the loop, as we set batch_size to the length of the array
        output_NLL = classifier_NLL(inputt.float())
        output_BCE = classifier_BCE(inputt.float())  
        preds_BCE =  classifier_BCE.output_to_label(output_BCE)
        preds_NLL = classifier_NLL.output_to_label(output_NLL)
    # we have ind "preds" a prediction for each point of XXYY. We're going to put theses values in ZZ
    compteur = 0 
    for ky in range(len(Y)):
        for kx in range(len(X)):
            ZZ_NLL[ky,kx] = preds_NLL[compteur]
            ZZ_BCE[ky,kx] = preds_BCE[compteur]
            compteur += 1
    # now we plot
    fig = plt.figure(figsize=(18,9))
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122)
    ax1.pcolormesh(XX,YY,ZZ_NLL)
    ax1.set_title(" NLL loss " )    
    ax2.pcolormesh(XX,YY,ZZ_BCE)
    ax2.set_title(" BCE loss " )
    [ ax.scatter(X_train[:, 0], X_train[:, 1], s=1, c=[ "r" if y == 1 else "b" for y in label_train ], cmap='bone') for ax in [ax1,ax2] ]
    if save_bool:
        plt.savefig("adam-heatmap.png",transparent=True)
    #########################################################
    # Classification using machine learning : NLLLoss / Adam
    #########################################################

    print(" computation time for NLL : ", computation_time_NLL )
    print(" computation time for BCE : ", computation_time_BCE )
    if not save_bool:
        plt.show()

