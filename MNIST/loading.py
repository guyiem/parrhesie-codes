import torchvision
import torch

def loading_datasets( repertory, normalize="by_image", download=False ):
    """
    function to load the MNIST train and test datasets, with an eventual normalization. Here normalization is directly proceeded without
    using torchvision.transforms tools.

    INPUTS
    ------
    rep : string
         the repertory where is stored the MNIST repertory
    normalize : string, or None
         if None, raw datasets are returned
         if "by_dataset", normalization is proceeded using the mean and std of all the training dataset.
         if "by_image", each image is normalized by its mean and its std.
    download : boolean
         if True, the dataset is downloaded if not found

    OUTPUTS:
      return train_set, the training dataset, and test_set, the testing dataset. Both are torchvision.datasets.mnist.MNIST
    """
    print(" ---------------------- ")
    print(" loading MNIST datasets ")
    print(" normalisation : ",normalize)
    print(" ---------------------- ")
    train_set = torchvision.datasets.MNIST(root=repertory, train=True, download=download )
    test_set = torchvision.datasets.MNIST( root=repertory, train=False, download=download)
    if normalize is None:
        return train_set, test_set
    elif normalize == "by_trainset":
        train_set.data = train_set.data.float()/255
        mean = train_set.data.mean()
        std = train_set.data.std()
        print(" mean and std of the training images before normalization : ",mean, std)
        train_set.data = (train_set.data - mean)/std
        test_set.data = (test_set.data.float()/255 - mean)/std
        return train_set, test_set
    elif normalize == "by_image":
        train_set.data = train_set.data.float()/255
        test_set.data = test_set.data.float()/255
        for dset in [train_set.data, test_set.data ]:
            mean = dset.data.mean(axis=(1,2))
            std = dset.data.std(axis=(1,2))
            dset.data = ((dset.data.T - mean)/std ).T
        return train_set, test_set
    else:
        raise ValueError(" normalize is an unknown kind : ", normalize )

def datasets_to_dataloader( train_set, test_set, batch_size=100, num_workers=4, shuffle=True):
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
    return train_loader, test_loader

def datasets_images_to_numpy( datasets):
    return datasets.data.numpy().astype(npy.float32)/255

