#coding: utf-8

from pathlib import Path

import torch
import torchvision
from torchvision import transforms
from torch.utils.data import Dataset, DataLoader
from sklearn.datasets import make_moons
import pickle
import numpy as npy

__all__ = [ "BasicDataset", "load_2circles", "load_moons", "load_MNIST", "datasets_to_dataloader" ]



class BasicDataset(Dataset):
    """half-moons points dataset"""
    def __init__(self, points, labels ):
        self.data = torch.from_numpy(points)
        self.targets = torch.from_numpy(labels)

    def __len__(self):
        return len(self.data)

    def __getitem__(self,idx):
        return self.data[idx], self.targets[idx]


###############################
# two and four circles dataset
###############################
def load_2circles(root_rep,create=False):
    rep = root_rep / "two_circles"
    try:
        with open( rep / "train.pck","rb") as f:
            X_train, train_label = pickle.load(f)
        with open( rep / "test.pck","rb") as f:
            X_test, test_label = pickle.load(f)
    except FileNotFoundError:
        if create:
            print(" 2 circles dataset doesn't exist. We generate and save it " )            
            x0 = -5
            x1 = 5
            y = 0
            ntrain = 10000
            ntest = 2000
            bernoulli_train = npy.random.binomial( size=ntrain, n=1, p=0.5 )
            X_train = ((1-bernoulli_train) * npy.random.normal(loc=(x0,y), scale=1, size=(ntrain,2)).T + bernoulli_train * npy.random.normal(loc=(x1,y), scale=1, size=(ntrain,2)).T).T
            train_label = bernoulli_train
            bernoulli_test = npy.random.binomial( size=ntest, n=1, p=0.5 )
            X_test = ((1-bernoulli_test) * npy.random.normal(loc=(x0,y), scale=1, size=(ntest,2)).T + bernoulli_test * npy.random.normal(loc=(x1,y), scale=1, size=(ntest,2)).T).T
            test_label = bernoulli_test
            with open( rep / "train.pck","wb") as f:
                pickle.dump([X_train, train_label], f)
            with open( rep / "test.pck","wb") as f:
                pickle.dump([X_test, test_label], f)
        else:
            raise FileNotFoundError(" 2 circles dataset not found ")
    return BasicDataset( X_train, train_label), BasicDataset( X_test, test_label )


###############################
# half moons dataset
###############################
def load_moons(root_rep,create=False):
    rep = root_rep / "moons"
    print(" loading or generating moons data ")
    # the training set
    try:
        with open( rep / "train.pck","rb") as f:            
            X_train, train_label = pickle.load(f)
    except FileNotFoundError:
        if create:
            print(" training set doesn't exist. We generate and save it " )
            X_train, train_label = make_moons(n_samples=10000, noise=0.1)
            with open( rep / "train.pck","wb") as f:
                pickle.dump([X_train, train_label], f)
        else:
            raise FileNotFoundError(" moons dataset not found ")
    # end of training set
    # the test set
    try:
        with open(rep / "test.pck","rb") as f:            
            X_test, test_label = pickle.load(f)
    except FileNotFoundError:
        if create:
            print( " test set doesn't exit. We generate and save it ")
            X_test, test_label = make_moons(n_samples=2000, noise=0.1)
            with open( rep / "test.pck","wb") as f:
                pickle.dump([X_test, test_label], f )
        else:
            raise FileNotFoundError(" moons dataset not found ")

    # end of the test set
    return BasicDataset( X_train, train_label), BasicDataset( X_test, test_label )
###############################
# half moons dataset
###############################


###############################
# MNIST
###############################
def load_MNIST( repertory, normalize="by_image", download=False ):
    """
    function to load the MNIST train and test datasets, with an eventual normalization. Here normalization is directly proceeded without
    using torchvision.transforms tools.

    INPUTS :
    --------
    rep : string
         the repertory where is stored the MNIST repertory
    normalize : string, or None
         if None, raw datasets are returned
         if "by_trainset", normalization is proceeded using the mean and std of all the training dataset.
         if "by_image", each image is normalized by its mean and its std.
    download : boolean
         if True, the dataset is downloaded if not found

    OUTPUTS :
    ---------
      return train_set, the training dataset, and test_set, the testing dataset. Both are torchvision.datasets.mnist.MNIST
    """
    print(" ---------------------- ")
    print(" loading MNIST datasets ")
    print(" normalisation : ",normalize)
    print(" ---------------------- ")
    train_set = torchvision.datasets.MNIST(root=repertory, train=True, download=download, transform=transforms.ToTensor() )
    test_set = torchvision.datasets.MNIST( root=repertory, train=False, download=download, transform=transforms.ToTensor() )
    if normalize is None:
        return train_set, test_set
    elif normalize == "by_trainset":
        train_set.data = train_set.data.float()/255
        mean = train_set.data.mean()
        std = train_set.data.std()
        print(" mean and std of the training images before normalization : ",mean, std)
        train_set.data = (train_set.data - mean)/std
        test_set.data = (test_set.data.float()/255 - mean)/std
        return train_set, test_set
    elif normalize == "by_image":
        train_set.data = train_set.data.float()/255
        test_set.data = test_set.data.float()/255
        for dset in [train_set.data, test_set.data ]:
            mean = dset.data.mean(axis=(1,2))
            std = dset.data.std(axis=(1,2))
            dset.data = ((dset.data.T - mean)/std ).T
        return train_set, test_set
    else:
        raise ValueError(" normalize is an unknown kind : ", normalize )
###############################
# end MNIST
###############################


def datasets_to_dataloader( train_set, test_set, batch_size=100, num_workers=4, shuffle=True):
    train_loader = torch.utils.data.DataLoader(train_set, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
    test_loader = torch.utils.data.DataLoader(test_set, batch_size=batch_size, shuffle=shuffle, num_workers=num_workers)
    return train_loader, test_loader


    
