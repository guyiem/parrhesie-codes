#coding: utf-8

import sys
import torch
from torch.utils.data import Dataset, DataLoader
from pamopy.loaders import *
from pathlib import Path
rep = Path(sys.argv[-1])  #Path("/Users/lusenn/Desktop/donnees-ml/")    

def test_load_moons():
    #train_set, test_set = load_moons(Path("."),create=False)
    print(" moons rep : ", rep)
    #train_set, test_set = load_moons(rep,create=True)
    train_set, test_set = load_moons(rep,create=False)
    x0,l0 = train_set[0]
    assert x0.shape == (2,), print( x0.shape )
    assert len(x0) == 2 , print( len(x0) )
    assert l0.shape == (), print( l0.shape)
    print(" end loading moons ")


def test_load_2circles():
    #train_set, test_set = load_moons(Path("."),create=False)
    print(" 2circles rep : ", rep)
    train_set, test_set = load_2circles(rep,create=True)
    train_set, test_set = load_2circles(rep,create=True)
    x0,l0 = train_set[0]
    assert x0.shape == (2,), print( x0.shape )
    assert len(x0) == 2 , print( len(x0) )
    assert l0.shape == (), print( l0.shape)
    print(" end loading 2circles ")

    

def test_load_MNIST():
    train_set, test_set = load_MNIST( rep )
    assert isinstance(train_set , Dataset )
    assert isinstance(train_set.data , torch.Tensor )
    train_set, test_set = load_MNIST( rep, normalize=None )
    assert isinstance(train_set , Dataset )
    assert isinstance(train_set.data , torch.Tensor )
    train_set, test_set = load_MNIST( rep, normalize="by_trainset" )
    assert isinstance(train_set , Dataset )
    assert isinstance(train_set.data , torch.Tensor )
    assert isinstance(train_set.targets , torch.Tensor )
    
def test_datasets_to_dataloader():
    train_set, test_set = load_MNIST( rep, normalize=None )
    datasets_to_dataloader( train_set, test_set )
    
