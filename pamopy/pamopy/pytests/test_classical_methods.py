#coding: utf-8

import sys
import torch
from torch.utils.data import Dataset, DataLoader
from pamopy.loaders import *
from pamopy.classicalmethods import *
from pathlib import Path

rep =  Path(sys.argv[-1]) #Path("/Users/lusenn/Desktop/donnees-ml/")


def test_k_nearest_neighbors():
    dist_func = torch.linalg.norm
    # a first test to check everything run fine
    X_to_locate = torch.randn((4,4))
    X_train = torch.randn((10,4,4))
    nears1 = k_nearest_neighbors( X_to_locate, X_train, 1, lambda x: dist_func(x,dim=(1,2))  )
    nears2 = k_nearest_neighbors( X_to_locate, X_train, 2, lambda x: dist_func(x,dim=(1,2))  )
    nears3 = k_nearest_neighbors( X_to_locate, X_train, 3, lambda x: dist_func(x,dim=(1,2))  )
    # a second test
    X_to_locate = 1.0 * torch.tensor([0,0])
    X_train =  1.0 * torch.tensor([[1,0],
                            [2,0],
                            [3,0],
                            [4,0]])
    print("\n"+ 42*"-" )
    print("--- RESULTS OF THE K_NEAREST_NEIGHBORS --- ")
    print( 42*"-" )
    print(" X_to_locate")
    print(" -----------")
    print(X_to_locate)
    print(" X_train")
    print(" -------")
    print(X_train)
    nears1 = k_nearest_neighbors( X_to_locate, X_train, 1, lambda x: dist_func(x, dim=(1,) )  )
    nears2 = k_nearest_neighbors( X_to_locate, X_train, 2, lambda x: dist_func(x, dim=(1,))  )
    nears3 = k_nearest_neighbors( X_to_locate, X_train, 3, lambda x: dist_func(x,dim=(1,))  )
    print("\n type and values of nears1, nears2, nears3")
    print(" -----------------------------------------")
    print(type(nears1))
    print(nears1,"\n")
    print(type(nears2))
    print(nears2,"\n")
    print(type(nears3))
    print(nears3,"\n")
    print("\n closest neighbors of : ", X_to_locate)
    print(" ----------------------")
    print(" 1_nearest_neighbor : \n ", X_train[ nears1.indices ] )
    print(" 2_nearest_neighbors : \n ", X_train[ nears2.indices ] )
    print(" 3_nearest_neighbors : \n ", X_train[ nears3.indices ] )
    assert (X_train[ nears1.indices ] == torch.tensor([[1.0, 0.0]])).all(), " 1_nearest_neighbor is incorrect"
    assert (X_train[ nears2.indices ] == torch.tensor([[1.0, 0.0], [2.0, 0.0]] )).all(), " 2_nearest_neighbors is incorrect"
    assert (X_train[ nears3.indices ] == torch.tensor([[1.0, 0.0], [2.0, 0.0], [3.0, 0.0]])).all(), " 1_nearest_neighbor is incorrect" 
                            

def test_label_by_knn():
    dist_func = torch.norm
    train_set, test_set = load_2circles(rep,create=False)
    X_to_locate0 = torch.tensor( [ -5.0, 0.0 ])
    X_to_locate1 = torch.tensor( [ 5.0, 0.0 ])
    X_to_locate2 = torch.tensor( [ 0.0, 0.0 ])
    best_targets0 = label_by_knn( X_to_locate0, train_set, 10, lambda x: dist_func(x,dim=(1,))  )
    best_targets1 = label_by_knn( X_to_locate1, train_set, 10, lambda x: dist_func(x,dim=(1,))  )
    best_targets2 = label_by_knn( X_to_locate2, train_set, 10, lambda x: dist_func(x,dim=(1,))  )
    assert best_targets0 == torch.tensor(0), " target for X_to_locate0 is incorrect "
    assert best_targets1 == torch.tensor(1), " target for X_to_locate1 is incorrect "
    print("\n"+ 42*"-" )
    print("--- RESULTS OF THE LABEL_BY_KNN --- ")
    print( 42*"-" )
    print(" 2-circles dataset : ")
    print(" ------------------- ")
    print(" target for (-5,0) : ", best_targets0 )
    print(" target for (5,0) : ", best_targets1 )
    print(" target for (0,0) : ", best_targets2 )

    
def test_means_by_labels():
    train_set, _ = load_2circles(rep,create=False)
    output = means_by_labels( train_set.data, train_set.targets, [torch.tensor(0),torch.tensor(1)], axis=0 )
    print("\n" + 42*"-" )
    print("--- RESULTS OF MEANS_BY_LABEL --- ")
    print( 42*"-" )
    print(" 2-circles dataset : ")
    print(" ------------------- ")
    print(" mean by label should be around (-5,0) and (5,0) ")
    print(output)
    assert torch.linalg.norm( output[0] - torch.tensor([-5.0, 0.0]) )<0.1, " mean for 0-label is not close enough of (-5.0, 0.0)"
    assert torch.linalg.norm( output[1] - torch.tensor([5.0, 0.0]) )<0.1, " mean for 1-label is not close enough of (5.0, 0.0)"

    
def test_nearest_mean():
    train_set, _ = load_2circles(rep,create=False)
    X0 = torch.tensor([-5.0, 0.0])
    X1 = torch.tensor([5.0, 0.0])
    lab0 = nearest_mean( X0, train_set, (torch.tensor(0), torch.tensor(1)), torch.norm )
    lab1 = nearest_mean( X1, train_set, (torch.tensor(0), torch.tensor(1)), torch.norm )
    print("\n"+ 42*"-" )
    print("--- RESULTS OF MEANS_BY_LABEL --- ")
    print( 42*"-" )
    print(" 2-circles dataset : ")
    print(" ------------------- ")
    print("0-label found is : ", lab0)
    print("1-label found is : ", lab1)
    assert lab0 == torch.tensor(0) , " lab0 is not equal to torch.tensor(0) but to : {}".format(lab0)
    assert lab1 == torch.tensor(1) , " lab0 is not equal to torch.tensor(1) but to : {}".format(lab1)
    
