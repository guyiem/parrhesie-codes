import torch
import numpy as npy

__all__ = [ "k_nearest_neighbors", "label_by_knn", "means_by_labels", "nearest_mean"  ]

##########################################################
# Classical classifier : nearest neighbors
##########################################################
# first a generic function returning, for a given "point" the nearest neighbors in a dataset
# then a classification function which attributes a label using nearest neighbors

def k_nearest_neighbors( X_to_locate, X_train, k, dist_func ):
    """
    Given a fixed point and a given set, computes the k nearest neighbors in this set.

    Inputs :
    -------
    X_to_locate : torch.Tensor, with floating_point dtype
        The point we are trying to locate.
    X_train : torch.Tensor, with floating_point dtype
        The set of points where we are looking for the neighbors.
        We must have X_to_locate.shape == X_train.shape[1:]
    k : int (positive)
        The number of neighbor we are looking for
    dist_func : the function we are using to compute distance. 
        Will be evaluated on X_to_locate - X_train

    Returns :
    -------
    A named tuple (values, indices) where the indices are the indices of the elements in X_train.
    """
    # checking type of inputs
    assert torch.is_tensor(X_to_locate), " X_to_locate isn't a tensor "
    assert torch.is_tensor(X_train), " X_train isn't a tensor "
    # checking dtype of tensors and shape
    assert X_to_locate.is_floating_point(), "X_to_locate doesn't cast to float "
    assert X_train.is_floating_point(), " in X_train is not floating point"
    assert X_to_locate.shape == X_train.shape[1:], " X_to_locate and X_train don't have compatible shapes"
    # end of check
    dist = dist_func( X_to_locate - X_train )
    return dist.topk( k, largest=False )

def label_by_knn( X_to_locate, train_set, k, dist_func  ):
    """
    For a fixed point and a labelled training set, gives a label for this point 
    using the prevailing label in the k nearest neighbors.

    Inputs :
    --------
    X_to_locate : torch.Tensor, with floating_point dtype
        The point we are trying to locate.
    train_set : Dataset
        The training set
    k : int (positive)
        The number of neighbor we are looking for
    dist_func : the function we are using to compute distance. 
        Will be evaluated on X_to_locate - X_train

    Return :
    -------
    a one element torch.Tensor containing the label

    """
    train_labels = train_set.targets
    X_train = train_set.data
    #TODO : change to a Dataset input parameter?
    assert ~train_labels.is_floating_point(), " labels doesn't cast as integer"
    #TODO : raise warnings if there isn't a obvious label? 
    knn = k_nearest_neighbors( X_to_locate, X_train, k, dist_func )
    best_labels, labels_count = train_labels[ knn.indices ].unique( return_counts=True )
    return best_labels[ labels_count.argmax() ]
##########################################################
# End of Classical classifier : nearest neighbors
##########################################################


##########################################################
# Classical classifier : means
##########################################################
def means_by_labels( data, data_labels, labels, axis=0 ):
    """
    Given labelled data, compute the mean value according to the label.

    Inputs :
    ------
    data : torch.Tensor, with floating_point dtype
    data_labels : torch.Tensor, with integer dtype
        The label corresponding to the data
    labels : Iterable (ideally a set?)
       The existing labels, each one unique
    axis : direction for the meaning process. 0 rows, 1 columns


    Return :
    -------
    A dict labelled by the int value of the labels.
    To each key (label) corresponds the given mean.
    
    """
    assert torch.is_tensor(data), " data isn't a tensor"
    assert torch.is_tensor(data_labels), " data_labels isn't a tensor"
    assert data.is_floating_point(), " data.dtype isn't a floating point"
    assert ~data_labels.is_floating_point(), " data_labels.dtype isn't a floating point"
    for lab in labels:
        assert torch.is_tensor(lab), "at least one label of labels isn't a tensor"
        assert ~lab.is_floating_point(), " at least one label of labels isn't an int"
    output = {}
    nd = len(data)
    indices = []
    for lab in labels:
        indices = [ i for i in range(nd) if data_labels[i] == lab ]
        output[lab.item()] = data[indices].mean(axis=axis)
       #output[lab] =  npy.array([ data[i].numpy() for i in range(nd) if data_labels[i] == lab  ]).mean() 
    return output


def nearest_mean( X_to_locate, train_set, labels, dist_func ):
    """
    For a fixed point and a labelled training set, gives a label for this point 
    using the closest labelled-mean value.

        Inputs :
    --------
    X_to_locate : torch.Tensor, with floating_point dtype
        The point we are trying to locate.
    train_set : Dataset
        The training set
    dist_func : the function we are using to compute distance. 
        Will be evaluated on X_to_locate - X_train


    Return :
    -------
    a one element torch.Tensor containing the label
    """
    assert torch.is_tensor(X_to_locate), " X_to_locate isn't a tensor "
    # checking dtype of tensors and shape
    assert X_to_locate.is_floating_point(), "X_to_locate doesn't cast to float "
    assert X_to_locate.shape == train_set.data.shape[1:], " X_to_locate and train_set.data don't have compatible shapes"
    means = means_by_labels( train_set.data, train_set.targets, labels )
    dist = {}
    for lab in labels:
        dist[lab.item()] = dist_func( X_to_locate -  means[lab.item()] )
    min_lab = min( dist, key=dist.get )
    return labels[ min_lab ]
##########################################################
# Classical classifier : means
##########################################################
