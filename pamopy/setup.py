#coding: utf-8

from setuptools import setup, find_packages

setup( 
    name='pamopy',
    packages=find_packages(include=["pamopy"]),

    #version=pamopy.__version__,


    author="bob", 
    author_email="",
 
    # Une description courte
    description="Models and loader for the parrhesie codes",
    #long_description=open('README.md').read(),

    install_requires= [ "numpy", "pandas", "matplotlib", "scipy", "torch", "torchvision", "sklearn" ],
    #include_package_data=True,
) 
